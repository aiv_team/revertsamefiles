﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace RevertSameFiles
{
    public partial class MainForm : Form
    {
        private readonly char ERROR = 'e';
        private readonly char OUT = 'o';

        private string folder;

        public MainForm()
        {
            InitializeComponent();

            DialogResult result = selectFolderDialog.ShowDialog();
            if (DialogResult.Cancel == result)
            {
                MessageBox.Show("Не выбрана папка, работа программы завершается", "Откат неизменённых файлов", MessageBoxButtons.OK);

                Environment.Exit(1);
            }

            folder = selectFolderDialog.SelectedPath;
        }

        protected override void OnShown(EventArgs e)
        {
            FolderLabel.ForeColor = System.Drawing.Color.OrangeRed;
            FolderLabel.Text = "Обработка папки: " + folder + " ...";

            var gitStatus = ProcessGitCommand(folder, "status");
            if (!String.IsNullOrWhiteSpace(gitStatus[ERROR]))
            {
                MessageBox.Show(gitStatus[ERROR], "Откат неизменённых файлов", MessageBoxButtons.OK);

                Environment.Exit(1);
            }

            InitializeBackgroundWorker();
            gitBackgroundWorker.RunWorkerAsync(gitStatus[OUT]);
        }

        private Dictionary<char, string> ProcessGitCommand(string folder, string args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                WorkingDirectory = folder,
                FileName = "git.exe",
                Arguments = args,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            Process process = Process.Start(startInfo);

            //* Read the output and the error
            var gitOutput = new Dictionary<char, string>
            {
                { OUT, process.StandardOutput.ReadToEnd() },
                { ERROR, process.StandardError.ReadToEnd() }
            };

            process.WaitForExit();

            return gitOutput;
        }

        private ArrayList ParseGitStatus(string gitStatus)
        {
            string[] lines = gitStatus.Split(new string[] { "\n", "\r\n", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            ArrayList changedFiles = new ArrayList();

            foreach (string line in lines)
            {
                if (line.Contains("modified:"))
                {
                    changedFiles.Add(line.Replace("modified:", "").Trim());
                }
            }

            return changedFiles;
        }

        private void InitializeBackgroundWorker()
        {
            gitBackgroundWorker.WorkerReportsProgress = true;
            gitBackgroundWorker.DoWork += new DoWorkEventHandler(GitBackgroundWorker_DoWork);
            gitBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GitBackgroundWorker_RunWorkerCompleted);
            gitBackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(GitBackgroundWorker_ProgressChanged);
        }

        private void GitBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ProcessFiles((string) e.Argument, worker, e);
        }

        private void GitBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            gitProgressBar.Value = e.ProgressPercentage;

            var gitOperationResult = (GitOperationResult) e.UserState;
            if (gitOperationResult.IsReverted)
            {
                revertedFileList.AppendText(gitOperationResult.FileName + "\r\n");
            }
            else
            {
                savedFilesList.AppendText(gitOperationResult.FileName + "\r\n");
            }
        }

        private void GitBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                FolderLabel.ForeColor = System.Drawing.Color.DarkGreen;
                FolderLabel.Text = "Обработка папки: " + folder + " завершена";
            }
        }

        private void ProcessFiles(string gitStatus, BackgroundWorker worker, DoWorkEventArgs e)
        {
            var fileList = ParseGitStatus(gitStatus);
            for (int i = 0; i < fileList.Count; i++)
            {
                var fileName = fileList[i];

                var gitOperationResult = new GitOperationResult
                {
                    FileName = (string) fileName
                };

                var diff = ProcessGitCommand(folder, "diff " + fileName)[OUT];

                if (String.IsNullOrWhiteSpace(diff))
                {
                    ProcessGitCommand(folder, "checkout " + fileName);
                    gitOperationResult.IsReverted = true;
                }
                else
                {
                    gitOperationResult.IsReverted = false;
                }

                worker.ReportProgress((int) ((float) (i + 1) / fileList.Count * 100), gitOperationResult);
            }
        }
    }

    class GitOperationResult
    {
        public string FileName { get; set; }
        public bool IsReverted { get; set; }
    }
}