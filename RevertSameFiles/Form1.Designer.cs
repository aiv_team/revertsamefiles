﻿namespace RevertSameFiles
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.selectFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.FolderLabel = new System.Windows.Forms.Label();
            this.savedFilesLabel = new System.Windows.Forms.Label();
            this.revertedFilesLabel = new System.Windows.Forms.Label();
            this.savedFilesList = new System.Windows.Forms.TextBox();
            this.revertedFileList = new System.Windows.Forms.TextBox();
            this.gitBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.gitProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // selectFolderDialog
            // 
            this.selectFolderDialog.Description = "Откат неизменённых файлов";
            this.selectFolderDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.selectFolderDialog.ShowNewFolderButton = false;
            // 
            // FolderLabel
            // 
            this.FolderLabel.AutoSize = true;
            this.FolderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.FolderLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.FolderLabel.Location = new System.Drawing.Point(12, 9);
            this.FolderLabel.Name = "FolderLabel";
            this.FolderLabel.Size = new System.Drawing.Size(43, 17);
            this.FolderLabel.TabIndex = 1;
            this.FolderLabel.Text = "label";
            // 
            // savedFilesLabel
            // 
            this.savedFilesLabel.AutoSize = true;
            this.savedFilesLabel.Location = new System.Drawing.Point(9, 71);
            this.savedFilesLabel.Name = "savedFilesLabel";
            this.savedFilesLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.savedFilesLabel.Size = new System.Drawing.Size(115, 13);
            this.savedFilesLabel.TabIndex = 3;
            this.savedFilesLabel.Text = "Изменённые файлы";
            // 
            // revertedFilesLabel
            // 
            this.revertedFilesLabel.AutoSize = true;
            this.revertedFilesLabel.Location = new System.Drawing.Point(799, 71);
            this.revertedFilesLabel.Name = "revertedFilesLabel";
            this.revertedFilesLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.revertedFilesLabel.Size = new System.Drawing.Size(209, 13);
            this.revertedFilesLabel.TabIndex = 4;
            this.revertedFilesLabel.Text = "Одинаковые файлы (откат изменений)";
            // 
            // savedFilesList
            // 
            this.savedFilesList.Location = new System.Drawing.Point(12, 87);
            this.savedFilesList.MaxLength = 65536;
            this.savedFilesList.Multiline = true;
            this.savedFilesList.Name = "savedFilesList";
            this.savedFilesList.ReadOnly = true;
            this.savedFilesList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.savedFilesList.Size = new System.Drawing.Size(770, 812);
            this.savedFilesList.TabIndex = 5;
            // 
            // revertedFileList
            // 
            this.revertedFileList.Location = new System.Drawing.Point(802, 87);
            this.revertedFileList.MaxLength = 65536;
            this.revertedFileList.Multiline = true;
            this.revertedFileList.Name = "revertedFileList";
            this.revertedFileList.ReadOnly = true;
            this.revertedFileList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.revertedFileList.Size = new System.Drawing.Size(770, 812);
            this.revertedFileList.TabIndex = 6;
            // 
            // gitBackgroundWorker
            // 
            this.gitBackgroundWorker.WorkerReportsProgress = true;
            // 
            // gitProgressBar
            // 
            this.gitProgressBar.Location = new System.Drawing.Point(12, 29);
            this.gitProgressBar.Name = "gitProgressBar";
            this.gitProgressBar.Size = new System.Drawing.Size(1560, 23);
            this.gitProgressBar.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 911);
            this.Controls.Add(this.gitProgressBar);
            this.Controls.Add(this.revertedFileList);
            this.Controls.Add(this.savedFilesList);
            this.Controls.Add(this.revertedFilesLabel);
            this.Controls.Add(this.savedFilesLabel);
            this.Controls.Add(this.FolderLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Откат неизменённых файлов";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog selectFolderDialog;
        private System.Windows.Forms.Label FolderLabel;
        private System.Windows.Forms.Label savedFilesLabel;
        private System.Windows.Forms.Label revertedFilesLabel;
        private System.Windows.Forms.TextBox savedFilesList;
        private System.Windows.Forms.TextBox revertedFileList;
        private System.ComponentModel.BackgroundWorker gitBackgroundWorker;
        private System.Windows.Forms.ProgressBar gitProgressBar;
    }
}

